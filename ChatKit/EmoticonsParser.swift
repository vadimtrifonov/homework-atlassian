import Foundation

final class EmoticonsParser: ParserType {
    
    private static let pattern = "\\([:alnum:]{1,15}\\)"
    
    func parse(input: String, completionHandler: [AnyObject] -> Void) {
        guard let regex = try? NSRegularExpression(pattern: EmoticonsParser.pattern, options: [.CaseInsensitive]) else {
            completionHandler([])
            return
        }
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
            let emoticons = input.substringsWithRegex(regex).map({ String($0.characters.dropFirst().dropLast()) })
            
            dispatch_async(dispatch_get_main_queue()) {
                completionHandler(emoticons)
            }
        }
    }
}