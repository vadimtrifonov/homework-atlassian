import Foundation

public final class MessageParser {
    
    private static let emptyResult = "{}"
    
    private let urlSession: NSURLSession
    private lazy var parsers: [String: ParserType] = { [unowned self] in
        ["mentions": MentionsParser(), "emoticons": EmoticonsParser(), "links": LinksParser(urlSession: self.urlSession)]
    }()
    
    public init(urlSession: NSURLSession) {
        self.urlSession = urlSession
    }
    
    public func parse(input: String, completionHandler: (json: String) -> Void) {
        var result = [String: AnyObject]()
        let group = dispatch_group_create()
        
        for (key, parser) in parsers {
            dispatch_group_enter(group)
            
            parser.parse(input) { output in
                if !output.isEmpty {
                    result[key] = output
                }
                dispatch_group_leave(group)
            }
        }
        
        dispatch_group_notify(group, dispatch_get_main_queue()) {
            completionHandler(json: result.toJSON() ?? MessageParser.emptyResult)
        }
    }
}

