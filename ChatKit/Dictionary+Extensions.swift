import Foundation

extension Dictionary where Key: StringLiteralConvertible, Value: AnyObject {
    
    func toJSON() -> String? {
        if let
            dictionary = (self as? AnyObject) as? [String: AnyObject],
            jsonData = try? NSJSONSerialization.dataWithJSONObject(dictionary, options: .PrettyPrinted),
            jsonString = String(data: jsonData, encoding: NSUTF8StringEncoding)
        {
            return jsonString
        }
        return nil
    }
}