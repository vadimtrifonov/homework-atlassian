import UIKit

extension String {
    
    func substringsWithRegex(regex: NSRegularExpression) -> [String] {
        let matches = regex.matchesInString(self, options: [], range: NSRange(location: 0, length: self.characters.count))
        return matches.map({ self.substringWithNSRange($0.range) })
    }
    
    func substringWithNSRange(nsRange: NSRange) -> String {
        let fromUTF16 = utf16.startIndex.advancedBy(nsRange.location, limit: utf16.endIndex)
        let toUTF16 = fromUTF16.advancedBy(nsRange.length, limit: utf16.endIndex)
        
        if let
            from = String.Index(fromUTF16, within: self),
            to = String.Index(toUTF16, within: self)
        {
            return self.substringWithRange(from ..< to)
        }
        return ""
    }
    
    func stripHTML() -> String {
        let options: [String: AnyObject] = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding]
        
        if let
            data = self.dataUsingEncoding(NSUTF8StringEncoding),
            strippedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil).string
        {
            return strippedString
        }
        return self
    }
}