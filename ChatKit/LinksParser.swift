import Foundation

final class LinksParser: ParserType {
    
    private let urlSession: NSURLSession
    
    init(urlSession: NSURLSession) {
        self.urlSession = urlSession
    }
    
    func parse(input: String, completionHandler: [AnyObject] -> Void)  {
        guard let detector = try? NSDataDetector(types: NSTextCheckingType.Link.rawValue) else {
            completionHandler([])
            return
        }
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
            let urls = input.substringsWithRegex(detector).flatMap(NSURL.init)
            
            self.linksForURLs(urls) { links in
                dispatch_async(dispatch_get_main_queue()) {
                    completionHandler(links)
                }
            }
        }
    }
    
    private func linksForURLs(urls: [NSURL], completionHandler: [[String: String]] -> Void) {
        var links = [[String: String]]()
        let group = dispatch_group_create()
        
        for url in urls {
            dispatch_group_enter(group)
            
            self.titleForURL(url) { title in
                if let title = title {
                    links.append(["url": url.absoluteString, "title": title])
                }
                dispatch_group_leave(group)
            }
        }
        
        dispatch_group_notify(group, dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
            completionHandler(links)
        }
    }

    private func titleForURL(url: NSURL, completionHandler: String? -> Void) {
        let task = urlSession.dataTaskWithURL(url) { data, response, error in
            if let
                data = data,
                page = String(data: data, encoding: NSUTF8StringEncoding)
            {
                completionHandler(self.titleFromPage(page))
                return
            }
            completionHandler(nil)
        }
        task.resume()
    }
    
    /* 
     This is a somewhat naive implementation, but it works well in most cases, outweighing benefits of the WebView-based implementation (especially in terms of space complexity).
     One weakness of this implementation is non-minified pages with commented out titles (which should be super rare).
    */
    private func titleFromPage(page: String) -> String? {
        if let
            titleStartIndex = page.rangeOfString("<title>", options: [.CaseInsensitiveSearch])?.endIndex,
            titleEndIndex = page.rangeOfString("</title>", options: [.CaseInsensitiveSearch])?.startIndex
        {
            return page.substringWithRange(titleStartIndex ..< titleEndIndex).stripHTML()
        }
        return nil
    }
}

