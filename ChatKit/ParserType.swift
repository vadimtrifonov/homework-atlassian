import Foundation

protocol ParserType {
    
    func parse(input: String, completionHandler: [AnyObject] -> Void)
}