import Foundation

final class MentionsParser: ParserType {
    
    private static let pattern = "(?!\\b)@\\w+"
        
    func parse(input: String, completionHandler: [AnyObject] -> Void) {
        guard let regex = try? NSRegularExpression(pattern: MentionsParser.pattern, options: [.CaseInsensitive]) else {
            completionHandler([])
            return
        }
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
            let mentions = input.substringsWithRegex(regex).map({ String($0.characters.dropFirst()) })
            
            dispatch_async(dispatch_get_main_queue()) {
                completionHandler(mentions)
            }
        }
    }
}