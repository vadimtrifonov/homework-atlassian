import UIKit
import ChatKit

final class TableViewController: UITableViewController, UITextViewDelegate {

    private struct Constants {
        static let defaultRowHeight: CGFloat = 88
        static let defaultAnimationDuration: NSTimeInterval = 0.3
    }
    
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var outputLabel: UILabel!
    @IBOutlet weak var outputActivityIndicator: UIActivityIndicatorView!
    
    private lazy var messageParser = MessageParser(urlSession: NSURLSession.sharedSession())
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = Constants.defaultRowHeight
    }
    
    @IBAction func done() {
        tableView.endEditing(true)
        
        guard let input = inputTextView.text else {
            return
        }
        
        outputActivityIndicator.startAnimating()
        outputLabel.alpha = 0
        
        messageParser.parse(input) { json in
            self.outputActivityIndicator.stopAnimating()
            UIView.animateWithDuration(Constants.defaultAnimationDuration) {
                self.outputLabel.alpha = 1
            }
            
            self.outputLabel.text = json
            self.updateTableViewRowHeights()
        }
    }
    
    private func updateTableViewRowHeights() {
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: - UITextViewDelegate
    
    func textViewDidChange(textView: UITextView) {
        self.updateTableViewRowHeights()
    }
}
