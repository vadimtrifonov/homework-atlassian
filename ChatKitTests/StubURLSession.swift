import Foundation

final class StubURLSession: NSURLSession {
        
    typealias ResponseType = (data: NSData?, urlResponse: NSURLResponse?, error: NSError?)
    
    var stubResponse: ResponseType = (data: nil, urlResponse: nil, error: nil)
    
    override func dataTaskWithURL(url: NSURL, completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void) -> NSURLSessionDataTask {
        return StubURLSessionDataTask(stubResponse: stubResponse, completionHandler: completionHandler)
    }
    
    private final class StubURLSessionDataTask: NSURLSessionDataTask {
        let stubResponse: ResponseType
        let completionHandler: ResponseType -> Void
        
        init(stubResponse: ResponseType, completionHandler: ResponseType -> Void) {
            self.stubResponse = stubResponse
            self.completionHandler = completionHandler
        }
        
        override func resume() {
            completionHandler(stubResponse)
        }
    }
}
