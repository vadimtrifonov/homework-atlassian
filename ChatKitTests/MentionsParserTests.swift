import XCTest
@testable import ChatKit

class MentionsParserTests: XCTestCase {
    
    private let mentionsParser = MentionsParser()
    
    func testWordMatch() {
        assert(mentionsParser, input: "@oliver @olivia_1986 @William_Shakespeare", output: ["oliver", "olivia_1986", "William_Shakespeare"], "Mentions are words starting with an @ character and ending with a non-word character")
    }
    
    func testWordBoundries() {
        assert(mentionsParser, input: " @jack@mia;@ava-(@thomas)", output: ["jack", "ava", "thomas"], "Mentions should have non-word boundries")
    }
    
    func testWhitespace() {
        assert(mentionsParser, input: "    @Amelia @james​ @Lucas  ", output: ["Amelia", "james", "Lucas"], "Mentions can have any whitespace between them")
    }
}
