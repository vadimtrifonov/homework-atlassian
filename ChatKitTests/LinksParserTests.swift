import XCTest
@testable import ChatKit

class LinksParserTests: XCTestCase {
    
    let urlSession = StubURLSession()
    var linksParser: LinksParser!
    
    override func setUp() {
        linksParser = LinksParser(urlSession: urlSession)
    }
    
    func testHasTitle() {
        let pageData = "<title>Stub Twitter title</title>".dataUsingEncoding(NSUTF8StringEncoding)
        urlSession.stubResponse = (data: pageData, urlResponse: nil, error: nil)
        
        assert(linksParser, input: "https://twitter.com", output: [["url": "https://twitter.com", "title": "Stub Twitter title"]])
    }
    
    func testNoTitle() {
        let pageData = "".dataUsingEncoding(NSUTF8StringEncoding)
        urlSession.stubResponse = (data: pageData, urlResponse: nil, error: nil)
        
        assert(linksParser, input: "https://unknown.url", output: [])
    }
    
    func testMultipleURLs() {
        let pageData = "<!DOCTYPE html><html><head><title>Some title</title></head></html>".dataUsingEncoding(NSUTF8StringEncoding)
        urlSession.stubResponse = (data: pageData, urlResponse: nil, error: nil)
        
        assert(linksParser, input: " https://atlassian.com https://twitter.com http://apple.com http:\\invalid", output: [
            ["url": "https://atlassian.com", "title": "Some title"],
            ["url": "https://twitter.com", "title": "Some title"],
            ["url": "http://apple.com", "title": "Some title"]
        ])
    }
}

