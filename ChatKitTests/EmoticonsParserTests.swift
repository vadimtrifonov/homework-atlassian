import XCTest
@testable import ChatKit

class EmoticonsParserTests: XCTestCase {
    
    private let emoticonsParser = EmoticonsParser()
    
    func testAlphanumericMatch() {
        assert(emoticonsParser, input: "(smile) (wow) (crying1) (not_sad)", output: ["smile", "wow", "crying1"], "Emoticons are alphanumeric strings contained in parentheses")
    }
    
    func testLength() {
        assert(emoticonsParser, input: "(1234567890123456)(123456789012345)(verylongemoticon)", output: ["123456789012345"], "Emoticons length should be no longer than 15 characters")
    }
    
    func testBoundries() {
        assert(emoticonsParser, input: "(smile())(*huh*)(tea;(apple)-((lol(", output: ["apple"], "Only open-closed parenthesis are valid emoticon boundries")
    }
    
    func testWhitespace() {
        assert(emoticonsParser, input: "   (smile)(yeah)​ (tea)  ", output: ["smile", "yeah", "tea"], "Emoticons can have any whitespace between them or none at all")
    }
}
