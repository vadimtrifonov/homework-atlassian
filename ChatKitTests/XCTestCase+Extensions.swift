import XCTest
@testable import ChatKit

extension XCTestCase {
    
    func assert<T: Equatable>(parser: ParserType, input: String, output expectedOutput: T, _ message: String = "") {
        let expectation = expectationWithDescription("Parsing should take less than 1 second")
        
        parser.parse(input) { output in
            guard let output = output as? T else {
                XCTFail("Unexpected output type")
                expectation.fulfill()
                return
            }
            
            XCTAssertEqual(output, expectedOutput, message)
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(1, handler: nil)
    }
}
