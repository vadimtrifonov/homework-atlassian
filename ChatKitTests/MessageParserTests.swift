import XCTest
@testable import ChatKit

class MessageParserTests: XCTestCase {
    
    let urlSession = StubURLSession()
    var messageParser: MessageParser!
    
    override func setUp() {
         messageParser = MessageParser(urlSession: urlSession)
    }

    func testAllTypesOfContentFound() {
        let pageData = "<title>Some title</title>".dataUsingEncoding(NSUTF8StringEncoding)
        urlSession.stubResponse = (data: pageData, urlResponse: nil, error: nil)
        
        assert(input: "@bob @john (success)(wow) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016", output: [
            "mentions": ["bob", "john"],
            "emoticons": ["success", "wow"],
            "links": [["url": "https://twitter.com/jdorfman/status/430511497475670016", "title": "Some title"]]
        ])
    }
    
    func testOnlyMentionsFound() {        
        assert(input: "@bob @john (success-wow) such a cool feature; htps:twitter.com/jdorfman/status/430511497475670016", output: [
            "mentions": ["bob", "john"]
        ])
    }
    
    func testOnlyEmoticonsFound() {
        assert(input: "bob john (success)(wow) such a cool feature;", output: [
            "emoticons": ["success", "wow"]
        ])
    }
    
    func testOnlyLinksFound() {
        let pageData = "<title>Some title</title>".dataUsingEncoding(NSUTF8StringEncoding)
        urlSession.stubResponse = (data: pageData, urlResponse: nil, error: nil)
        
        assert(input: "bob@john (success wow) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016", output: [
            "links": [["url": "https://twitter.com/jdorfman/status/430511497475670016", "title": "Some title"]]
        ])
    }
    
    func testNothingFound() {
        let expectation = expectationWithDescription("Parsing should take less than 1 second")
        messageParser.parse("bob@john (success wow) such a cool feature;") { output in
            XCTAssertEqual(output, "{\n\n}")
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(1, handler: nil)
    }
    
    private func assert<T: Equatable>(input input: String, output expectedOutput: T) {
        let expectation = expectationWithDescription("Parsing should take less than 1 second")
        
        messageParser.parse(input) { output in
            guard let output = (try? NSJSONSerialization.JSONObjectWithData(output.dataUsingEncoding(NSUTF8StringEncoding) ?? NSData(), options: [])) as? T else {
                XCTFail("Unexpected output type")
                expectation.fulfill()
                return
            }
            
            XCTAssertEqual(output, expectedOutput)
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(1, handler: nil)
    }
}
