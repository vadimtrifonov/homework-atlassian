# README #

This is a homework for Atlassian.

### What's included ###

* A basic app for the demonstration of the solution.
* The ChatKit framework that implements the solution.

### Remarks ###

* No external frameworks were used.
* The ChatKit is covered with tests.